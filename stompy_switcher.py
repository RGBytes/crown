#!/usr/bin/env python3
import pexpect
import os

"""Animations
00 => OwO-Look
01 => OwO
02 => Rave
03 => thinkgif
04 => trippy
05 => Nyan
06 => Hat
07 => ezgif
08 => E00HoOd
09 => 3
10 => animation
11 => animation1
12 => 5
13 => animation3
14 => animation4
15 => animation5
16 => animation6
17 => animation7
18 => animation8
19 => animation9
"""


USER_HOME = os.environ["HOME"]
LOOP_ANIMATIONS = (
    USER_HOME
    + "rpi-rgb-led-matrix/utils/led-image-viewer -f -t15 --led-chain=16 --led-slowdown-gpio=4 --led-pixel-mapper=U-mapper --led-pwm-bits=5 --led-pwm-lsb-nanoseconds=300 --led-pwm-dither-bits=2 /home/pi/rpi-rgb-led-matrix/utils/streams/*.stream"
)


def constrcut_loop():
    files = [
        USER_HOME + "rpi-rgb-led-matrix/utils/streams/%s" % f
        for f in os.listdir(USER_HOME + "rpi-rgb-led-matrix/utils/streams")
    ]
    return (
        USER_HOME
        + "rpi-rgb-led-matrix/utils/led-image-viewer -f -t15 --led-chain=16 --led-slowdown-gpio=4 --led-pixel-mapper=U-mapper --led-pwm-bits=5 --led-pwm-lsb-nanoseconds=300 --led-pwm-dither-bits=2 %s' % ' ".join(
            files
        )
    )


def construct_command(file_name: str):
    return (
        USER_HOME
        + "rpi-rgb-led-matrix/utils/led-image-viewer -f --led-chain=16 --led-slowdown-gpio=4 --led-pixel-mapper=U-mapper --led-pwm-bits=5 --led-pwm-lsb-nanoseconds=300 --led-pwm-dither-bits=2 /home/pi/rpi-rgb-led-matrix/utils/streams/%s.stream"
        % file_name
    )


def kill_process(process: pexpect.spawn):
    if process:
        process.sendcontrol("c")


def safe_shutdown(process=None):
    if process:
        kill_process(process)
    os.system("shutdown -h now")


def change_to(file_name: str, previous=None):
    try:
        if file_name == "*":
            to_run = constrcut_loop()
        elif os.path.isfile(
            USER_HOME + "rpi-rgb-led-matrix/utils/streams/%s.stream" % file_name
        ):
            to_run = construct_command(file_name)
        else:
            return previous
        kill_process(previous)
        return pexpect.spawn(to_run, timeout=None)
    except Exception as e:
        print(e)


def main():
    process = change_to("0")
    while True:
        try:
            command = input("Input file number: ")
            if command == "754272":
                safe_shutdown(process)
            process = change_to(command, process)
        except KeyboardInterrupt:
            kill_process(process)
            break


if __name__ == "__main__":
    main()
